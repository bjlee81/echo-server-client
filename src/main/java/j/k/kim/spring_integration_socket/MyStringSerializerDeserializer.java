package j.k.kim.spring_integration_socket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.serializer.Deserializer;
import org.springframework.core.serializer.Serializer;

public class MyStringSerializerDeserializer implements Serializer<String>, Deserializer<String> {

	private Charset myCharset = Charset.forName("UTF-8");
	
	@Override
	public void serialize(String object, OutputStream outputStream) throws IOException {
		outputStream.write(object.getBytes(myCharset));

		outputStream.flush();
	}

	@Override
	public String deserialize(InputStream inputStream) throws IOException {
		return parseString(inputStream, -1);
	}

	private String parseString(InputStream inputStream, int length) throws IOException {
		StringBuilder builder = new StringBuilder();
		int c;

		int readSize = length;
		if (length < 0) {
			readSize = inputStream.available();
		}
		
		for (int i = 0; i < readSize; ++i) {
			c = inputStream.read();
			if (c < 0) {
				break;
			}
			builder.append((char)c);
		}
		

		return builder.toString();
	}
	
}
