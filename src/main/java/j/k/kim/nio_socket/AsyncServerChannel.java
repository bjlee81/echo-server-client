package j.k.kim.nio_socket;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.nio.charset.Charset;
import java.util.concurrent.CountDownLatch;

public class AsyncServerChannel {

	private AsynchronousServerSocketChannel serverChannel;
	private AsynchronousSocketChannel clientSocketChannel;
	
	private Charset myCharset;
	private CountDownLatch latch;
	
	public AsyncServerChannel() throws IOException {
		serverChannel = AsynchronousServerSocketChannel.open();
		
		serverChannel.bind(new InetSocketAddress(12345));
		
		myCharset = Charset.forName("UTF-8");
		latch = new CountDownLatch(1);
	}

	public void doRun() {
		while (true) {
			System.out.println("server start");
			
			serverChannel.accept(null, new CompletionHandler<AsynchronousSocketChannel, Void>() {

				@Override
				public void completed(AsynchronousSocketChannel result, Void attachment) {
					if (serverChannel.isOpen()) {
						serverChannel.accept(null, this);
					}
					clientSocketChannel = result;
					
					if (clientSocketChannel != null && clientSocketChannel.isOpen() ) {
						echoHandler(clientSocketChannel);
					}
				}

				@Override
				public void failed(Throwable exc, Void attachment) {
					
				}
			});
			
			try {
				latch.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void echoHandler(AsynchronousSocketChannel clientSocketChannel) {
		ByteBuffer readBuffer = ByteBuffer.allocate(1024);
		clientSocketChannel.read(readBuffer, null, new CompletionHandler<Integer, Void>() {

			@Override
			public void completed(Integer result, Void attachment) {
				if (result < 0) {
					return;
				}
				readBuffer.flip();
				
				byte[] byteArr = new byte[result];
				readBuffer.get(byteArr);
				readBuffer.clear();
				
				String readString = new String(byteArr, myCharset);
				String writeString = "echo:" + readString;
				ByteBuffer writeBuffer = ByteBuffer.wrap(writeString.getBytes(myCharset));
				clientSocketChannel.write(writeBuffer);
				writeBuffer.clear();
				System.out.println("Server Read[" + readString + "]");
				System.out.println("Server Write[" + writeString + "]");
			}

			@Override
			public void failed(Throwable exc, Void attachment) {
				
			}
		});
	}

	
	public static void main(String[] args) throws IOException {
		AsyncServerChannel myServer = new AsyncServerChannel();
		myServer.doRun();
	}


	
}
